from datetime import timedelta
import random
import string
import time


def longestCommonPrefix(strings: list) -> str:
    if not strings:
        return ""
    for i in range(len(strings[0])):
        char = strings[0][i]
        for j in range(1, len(strings)):
            if i == len(strings[j]) or strings[j][i] != char:
                return strings[0][:i]
    return strings[0]


def randomString(string_length=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(string_length))


def random_list(list_size=50):
    """Generate a random list of fixed length """
    r_list = []
    for j in range(list_size):
        r_list.append(randomString())
    return r_list


def format_result(result):
    seconds = result
    microseconds = (result * 1000000) % 1000000
    output = timedelta(0, seconds, microseconds)
    return output


def main(list_size: int):
    _lcp, _lcp_dvq, start = {}, {}, 0
    for i in range(1, 10):
        random_l = random_list(list_size=list_size * i)
        start = time.time()
        longestCommonPrefix(random_l)
    A = "longest_Common_pbl " + str(format_result(time.time()-start))
    return A


if __name__ == '__main__':
    print(main(list_size=500))
