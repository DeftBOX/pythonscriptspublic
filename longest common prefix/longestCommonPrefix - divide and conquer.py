from datetime import timedelta
import random
import string
import time


def _longestCommonPrefix(strs):
    def commonPrefix(left, right):
        min_len = min(len(left), len(right))
        for i in range(min_len):
            if left[i] != right[i]:
                return left[:i]
        return left[:min_len]

    def find_longestCommonPrefix(strs, left_index, right_index):
        if left_index == right_index:
            return strs[left_index]
        # recursive call
        else:
            mid_index = (left_index + right_index) // 2
            lcpLeft = find_longestCommonPrefix(strs, left_index, mid_index)
            lcpRight = find_longestCommonPrefix(strs, mid_index + 1, right_index)
            return commonPrefix(lcpLeft, lcpRight)

    if not strs: return ""
    return find_longestCommonPrefix(strs, 0, len(strs) - 1)


def randomString(string_length=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(string_length))


def random_list(list_size=50):
    """Generate a random list of fixed length """
    r_list = []
    for j in range(list_size):
        r_list.append(randomString())
    return r_list


def format_result(result):
    seconds = result
    microseconds = (result * 1000000) % 1000000
    output = timedelta(0, seconds, microseconds)
    return output


def main(list_size: int):
    start = 0
    for i in range(1, 10):
        random_l = random_list(list_size=list_size * i)
        start = time.time()
        _longestCommonPrefix(random_l)
    A = "longest_Common_dnq " + str(format_result(time.time()-start))
    return A


if __name__ == '__main__':
    print(main(list_size=500))
