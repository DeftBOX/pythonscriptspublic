#Implementation of Rabin Miller Algorithm aka Primality Test
#12 Aug 2019
#Dhaval Vaishnani - Pls write to us at connect@deftboxsolutions.com for any clarification or for custom editing.

import numpy as np
from random import randrange
import matplotlib.pyplot as plt


def miller_rabin(n, k=10):
    if n == 2:
        return True
    if not n & 1:
        return False

    def check(a, s, d, n):
        x = pow(a, d, n)
        # print('%d ----> %d' % (a, x))
        if x == 1:
            return True
        for i in range(s - 1):
            if x == n - 1:
                return True
            x = pow(x, 2, n)
            # print('%d ----> %d' % (a, x))
        return x == n - 1

    s = 0
    d = n - 1

    while d % 2 == 0:
        d >>= 1
        s += 1

    for i in range(k):
        a = randrange(2, n - 1)
        if not check(a, s, d, n):
            return False
    return True


def main(up_limit, down_limit):
    check = None
    _up_limit, data, sum = up_limit, {}, 0
    while True:
        for i in range(_up_limit, int(round(_up_limit + 100, -2))):
            if miller_rabin(n=i):
                sum += 1
        data[str(_up_limit) + '-' + str(int(round(_up_limit + 100, -2)))] = sum
        sum = 0
        if not check:
            check = 1
            _up_limit += 101
        else:
            _up_limit += 100
        if int(round(_up_limit, -2)) == down_limit:
            break
    return data


data = main(10000, 11000)

objects = list(data.keys())
y_pos = np.arange(len(objects))
performance = list(data.values())

plt.bar(y_pos, performance, align='center', alpha=0.5)
plt.xticks(y_pos, objects, rotation=90)
plt.title('Analysis')
plt.show()
