1) Need you to scrape every gallery name from this webpage: https://www.artsy.net/galleries-a-z

2) Then, you need to provide an email for each gallery.
You can do this by clicking on the gallery name and then clicking on the "contact" tab and then clicking on "Contact X Gallery via Email"
Example: https://www.artsy.net/lcd-gallery/contact

3) Finally, you need to scrape the names of the artists that each gallery has.
You can find this information by clicking on the artist tab and a grid of the names will pop up.
Example: https://www.artsy.net/lcd-gallery/artists
Please put into excel spreadsheet: 1st Column = Name of Gallery, 2nd Column = Email, 3rd Column = Names of artists 
the gallery sells separated​ by commas