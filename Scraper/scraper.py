import aiohttp
from bs4 import BeautifulSoup
import asyncio
from openpyxl import Workbook
import re

BASE_DIR = 'https://www.artsy.net/galleries-a-z'
API = 'https://api.artsy.net/api/v1/partner'

token = ''


async def _get(session, a, ws):
    sub_url = API + a.get('href')

    name = a.getText()

    header = {
        'X-XAPP-TOKEN': token
    }

    async with session.get(sub_url, headers=header) as resp:
        data = await resp.json()

    email = data.get('email', '')

    async with session.get(f'{sub_url}/partner_artists', headers=header) as resp:
        data = await resp.json()

    all_artists = []

    for artists in data:
        try:
            artist = artists['artist']['name']
        except TypeError:
            artist = ''
        all_artists.append(artist)

    all_artists = ",".join(all_artists)
    ws.append([name, email, all_artists])


async def main():
    global token

    session = aiohttp.ClientSession()

    wb = Workbook()
    ws = wb.active

    async with session.get(BASE_DIR) as resp:
        html_data = await resp.text()
        if 'ARTSY_XAPP_TOKEN'in await resp.text():
            token = re.search(r"\"ARTSY_XAPP_TOKEN\":\"([^\"]+)\"", html_data).group(1)
        soup = BeautifulSoup(html_data, features="html.parser")

    total_task = []

    for div in soup.find_all("li", {"class": "a-to-z-item"}):
        total_task.append(_get(session, a=div.a, ws=ws))

    await asyncio.gather(*total_task)

    wb.save("sample.xlsx")
    await session.close()


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
loop.close()
