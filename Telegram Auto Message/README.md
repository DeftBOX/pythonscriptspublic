Telegram Auto Message 
=====================

These scripts uses Telethon library to interact with Telegram's API as a user account.

Pre-requisite
-------------

  pip3 install telethon


How to Use 
----------
You have to create your API on my.telegram.org first

    # These example values won't work. You must get your own api_id and
    # api_hash from https://my.telegram.org, under API Development.
  	
  	api_id = YOUR_API_ID				 # No quotes
	api_hash = 'YOUR_API_HASH'			 # API HASH in quotes
	phone = 'YOUR MOBILE NUMNER WITH +'  # e.g. '+11234567890'

Run first getuserfromgroup.py and it will create member.CSV with telegram user id.

You can update the message in sendmessage.py
messages= ["Hi {}, What's up? would you like to join our game channel https://t.me/todaysgamerz "]

Then you can run sendmessage.py to send the message.


Questions
---------
Please write to us at connect@deftboxsolutions.com for any questions or modifications.

Thanks and happy sending.
